﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.Model
{
    public enum Estado
    {
        Disponible = 1,
        Prestado = 2,
        DeBaja = 3
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Biblioteca.UI.Controllers
{
    public class LibrosDeBajaController : Controller
    {
        // GET: LibrosDeBaja
        public ActionResult Index()
        {
            return View();
        }

        // GET: LibrosDeBaja/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LibrosDeBaja/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LibrosDeBaja/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LibrosDeBaja/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LibrosDeBaja/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LibrosDeBaja/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LibrosDeBaja/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

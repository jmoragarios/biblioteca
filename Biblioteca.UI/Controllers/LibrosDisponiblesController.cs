﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Biblioteca.UI.Controllers
{
    public class LibrosDisponiblesController : Controller
    {
        // GET: LibrosDisponibles
        public ActionResult Index()
        {
            List<Biblioteca.Model.Libro> laListaDeLibrosDisponibles;
            Biblioteca.Business.GestorDeLibros elGestor = new Business.GestorDeLibros();

            laListaDeLibrosDisponibles = elGestor.ListarLosLibrosDisponiles();

            return View(laListaDeLibrosDisponibles);
        }

        // GET: LibrosDisponibles/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LibrosDisponibles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LibrosDisponibles/Create
        [HttpPost]
        public ActionResult Create(Biblioteca.Model.Libro libro)
        {
            try
            {
                Biblioteca.Business.GestorDeLibros elGestor = new Business.GestorDeLibros();
                elGestor.Agregar(libro);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LibrosDisponibles/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LibrosDisponibles/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LibrosDisponibles/Delete/5
        public ActionResult Prestar(int id)
        {
            Biblioteca.Business.GestorDeLibros elGestor = new Business.GestorDeLibros();
            Biblioteca.Model.Libro elLibro;

            elLibro = elGestor.ObtenerLibroPorId(id);

            return View(elLibro);
        }

        // POST: LibrosDisponibles/Delete/5
        [HttpPost]
        public ActionResult Prestar(Biblioteca.Model.Libro libro)
        {
            try
            {
                Biblioteca.Business.GestorDeLibros elGestor = new Business.GestorDeLibros();
                elGestor.Prestar(libro);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

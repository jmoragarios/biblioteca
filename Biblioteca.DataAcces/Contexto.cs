﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Biblioteca.DataAcces
{
    public class Contexto:DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Biblioteca.Model.Libro>().ToTable("Libros");
        }

        public DbSet<Biblioteca.Model.Libro> Libros { get; set; }
    }
}

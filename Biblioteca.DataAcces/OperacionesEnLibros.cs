﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Biblioteca.Model;

namespace Biblioteca.DataAcces
{
   public class OperacionesEnLibros
    {
        public void Agregar(Biblioteca.Model.Libro libro)
        {
            var db = new Contexto();
            db.Libros.Add(libro);
            db.Entry(libro).State = System.Data.Entity.EntityState.Added;
            db.SaveChanges();
        }


        public List<Biblioteca.Model.Libro> ListarLibrosDisponibles()
        {

            var db = new Contexto();

            var resultado = from c in db.Libros
                            where c.Estado == Model.Estado.Disponible 

                            select c;

            return resultado.ToList();
        }


        public List<Biblioteca.Model.Libro> ListarTodosLosLibros()
        {
            var db = new Contexto();

            return db.Libros.ToList();
        }

        public List<Libro> ListarLibrosPrestados()
        {
            var db = new Contexto();

            var resultado = from c in db.Libros
                            where c.Estado == Model.Estado.Prestado

                            select c;

            return resultado.ToList();
        }


        public Biblioteca.Model.Libro ObtenerLibroPorId(int Id)
        {

            var db = new Contexto();
            var resultado = db.Libros.Find(Id);

            return resultado;
        }

        public void Editar(Libro libro)
        {
            var valorBd = ObtenerLibroPorId(libro.Id);
            var db = new Contexto();

            valorBd.Nombre = libro.Nombre;
            valorBd.Autor = libro.Autor;
            valorBd.Estado = libro.Estado;

            db.Entry(valorBd).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }
    }
}

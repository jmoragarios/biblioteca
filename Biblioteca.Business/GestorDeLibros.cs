﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Biblioteca.Model;

namespace Biblioteca.Business
{
    public class GestorDeLibros
    {
       public List<Biblioteca.Model.Libro> ListarLosLibrosDisponiles()
        {
            List<Biblioteca.Model.Libro> laLista = new List<Model.Libro>();
            Biblioteca.DataAcces.OperacionesEnLibros operaciones = new DataAcces.OperacionesEnLibros();

            laLista = operaciones.ListarLibrosDisponibles();

            return laLista;

        }

        public List<Libro> ListarLosLibrosPrestados()
        {
            List<Biblioteca.Model.Libro> laLista = new List<Model.Libro>();
            Biblioteca.DataAcces.OperacionesEnLibros operaciones = new DataAcces.OperacionesEnLibros();

            laLista = operaciones.ListarLibrosPrestados();

            return laLista;

        }

        public void Agregar(Biblioteca.Model.Libro libro)
        {
            Biblioteca.DataAcces.OperacionesEnLibros operaciones = new DataAcces.OperacionesEnLibros();
            libro.Estado = Biblioteca.Model.Estado.Disponible;
            operaciones.Agregar(libro);
        }

        public Biblioteca.Model.Libro ObtenerLibroPorId(int Id)
        {
            Biblioteca.DataAcces.OperacionesEnLibros operaciones = new DataAcces.OperacionesEnLibros();

            return operaciones.ObtenerLibroPorId(Id);
        }

        public void Editar(Libro libro)
        {
            Biblioteca.DataAcces.OperacionesEnLibros operaciones = new DataAcces.OperacionesEnLibros();

             operaciones.Editar(libro);
        }

        public void Prestar(Libro libro)
        {
            Biblioteca.DataAcces.OperacionesEnLibros operaciones = new DataAcces.OperacionesEnLibros();
            libro.Estado = Estado.Prestado;
            operaciones.Editar(libro);
        }

    }
}
